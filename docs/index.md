# A propos de moi



Salut ! Je suis Anthony Millour. Etudiant en Master 1 en Architecture à la **_Cambre Horta ULB_**, je suis actuellement l'option **Design et Architecture**.  

Ici je détaillerai mon parcours personnel, mais aussi mon apprentissage tout au long de l'option Design. Pour voir plus de projets n'hésite pas à consulter [mon portfolio](https://amillour.github.io/index.html).


![](images/Selfie_2.jpg)

## Mon Parcours

Après l’obtention de mon Baccalauréat Scientifique en 2010, attiré par les Arts, j’ai rejoint l’école des _Beaux Arts de Brest_ (_**ESAB**_). Alternant entre **Art** et **Design**, j’y ai appris à concevoir un projet, à donner forme à une intention, à travers une multitude de médium tels que le dessin, la peinture, le volume.  
D’autres formations plus techniques m’y ont été dispensé : design graphique, modélisation 3D.  
Une première expérience du projet architectural m’y a été donné.  

En 2014, j’ai ressenti le besoin d’un apprentissage plus concret, où la valeur de ce qui est produit ne peut être porté à subjectivité. Quelque chose de profondément essentiel. Je me suis alors tourné vers l’**Agriculture** – comme j’aurai pu me tourner vers la Charpente, la Ferronnerie ou la Mécanique automobile.

Mes études en **Architecture** depuis lors, sont comme une synthèse de ces deux expériences. Elles sont pour moi l’occasion de mêler mes ambitions artistiques et fonctionnelles.

## PRATONE

![Pratone au musée du design](images/Pratone/PRATONE_1.jpg)

En visite au [ADAM Brussels Design Museum](http://designmuseum.brussels/), mon regard s'est porté sur **PRATONE**. Conçu par les trois designers Pietro Derossi, Giorgio Ceretti et Riccardo Rosso, il est un objet iconique du design Radical italien des années 70.


### Pourquoi ce choix ?

* L’objet est **incompréhensible** au premier regard. Sa forme, sa matérialité, sa fonction, tout nous échappe. C’est sans doute ce qui me l’a rendu fascinant. On veut le toucher, l’expérimenter pour en saisir le sens. On finit par comprendre que l’objet "peut" éventuellement nous servir d’assise où se lover.

* On devine un carré d’herbe, mais le saut d’échelle, ainsi que l’emploi d’un matériau artificiel, produisent un déplacement de la signification. Ce **détournement** en fait un objet pop, vivant, ludique. Pratone relève plus de la sculpture que du mobilier.

* C’est un **objet courageux** : dès sa conception il est voué à "l’échec". Il est impensable qu’il soit tiré en grande série ; sa place est davantage dans les musées que dans les salons.  
Provocateur, il est un bras d’honneur dressé face au design classique capitaliste.

## Exemples de Projet

### L'Escault

![Projet d'équipement sportif + logements, _Rue de l'Escault_](images/l_Escault.jpg)

Dans un [quartier](https://www.google.com/maps/place/Rue+de+l'Escaut,+1080+Molenbeek-Saint-Jean/@50.8686764,4.3407187,856m/data=!3m1!1e3!4m5!3m4!1s0x47c3c395c74f44bd:0x172a82d4986e5a9c!8m2!3d50.8675024!4d4.342) au passé industriel au Nord de Bruxelles, la zone de _Tour&Taxis_ bénéficie d'une large revitalisation. Un grand parc remplace l'ancien terrain vague de la gare de marchandise, et rejoint la Coulée Verte. Logements, commerces, équipements publics et institutions y fleurissent. Le site dont on détournait jusqu'à lors le regard, devient l'hépicentre de la mutation du quartier.

Le projet rue de l'Escault se veut comme une **porte d'entrée** vers le parc. La parcelle accueille un centre sportif ainsi qu'un immeuble d'habitation. Le rez-de-chaussé tout en transparence, perméable à la vue mais aussi aux circulation, fait le lien entre la rue (la ville) et le parc.

