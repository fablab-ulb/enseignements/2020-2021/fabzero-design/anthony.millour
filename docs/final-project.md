# Final Project

![1](images/projet%20final/1.jpg)

L’objet s’inspire librement de PRATONE, un fauteuil aux allures de carrée de pelouse vert pétant, élément iconique du design radical italien des années 70. Cet objet sculptural joue du paradoxe entre son inspiration d’un élément naturel et sa production artificiel. Il tend aussi à briser le fonctionnalisme du design moderne qui le précédait, en faisant de la Poésie et du Jeu ses maîtres-mots.
Ce luminaire dynamique tire sa forme des méduses qui dérivent çà et là dans les océans. L’objet s’invite à flotter dans l’espace dans lequel vous l’aurez installé. Ses bras tentaculaires déformables permettent de l’adapter à nombre de situations : il est aussi bien possible de le fixer au rebord d’une table, d’un bureau, que de l’enrouler à une rambarde, ou encore que de le poser directement sur une surface. Le corps translucide de l’objet laisse apparaître couche après couche les chemins par lesquels passa l’impression 3D pour sa réalisation, dans ses limites comme dans ses succès : une carapace expressive, qui participe de la poétique de la méduse.

## Liens vers les fichiers STL

Fichier STL du corps translucide entier téléchargeable [ici](images/projet%20final/Méduse_corps_transparent.stl)

Fichiers STL du corps translucide décomposé en 3 parties téléchargeable
 - sommet [ici](images/projet%20final/Medusa_test_sommet_1.stl)
 - milieu [ici](images/projet%20final/Corps23.5.stl)
 - bas [ici](images/projet%20final/Corps_3eme_partie.stl)

Fichier STL du corps noir téléchargeable [ici](images/projet%20final/Méduse_partie_ventrale_noire.stl)

Fichier STL des 3 éléments de fixation partie noire/partie translucide téléchargeable [ici](images/projet%20final/accroche.stl)

Fichier STL de la fixation douille/câbles téléchargeable [ici](images/projet%20final/Corps2.stl)


# Tests préalables

## Une méduse c’est quoi ?

### a.Forme et composition

![2](images/projet%20final/anatomie_méduse.jpg)

### b.Transparence/translucidité

![](images/projet%20final/3.jpg)

### c.Mouvement

![](images/projet%20final/4.jpg)

## Phase 1

L’objet premier a été de trouver comment réaliser la lampe médusée à l’aide des outils disponibles dans notre Fablab. L’objectif est de concevoir un objet, mais aussi de permettre sa reproductibilité aux quelques 2000 autres Fablabs existants. Les outils sont donc :
-	L’impression 3D : l’imprimante disponible dans notre Fablab est la Prusa i3 MK3S+
-	La découpe Laser : la Lasersaur chez nous
-	La Shaper
-	Le thermoformage

Une première série d’éléments ont été dessiner via SketchUp :

![](images/projet%20final/5.jpg)

Test 1 :

![](images/projet%20final/8.jpg)

### a. Un test d’abat-jour a été réalisé :

![](images/projet%20final/6.jpg)
![](images/projet%20final/7.jpg)

Feuilles de polypropylène de 1mm d’épaisseur découpée au laser. Assemblage sommital à l’aide d’une pièce réalisé en impression 3D. Assemblage des arrêtes à l’aide de ficelle.

### b. Pour le pied du luminaire plusieurs hypothèses ont été formulées, nous retiendront ces deux ci :
 1. Découpe à la Shaper d’éléments en bois

 2. Tentacules imprimés en plastique bleu

### c. Compte rendu de l’essai
- objet trop construit : trop de techniques et de matériaux différents utilisés, éléments d’assemblages trop visibles. Par exemple, les liens en ficelles prennent trop de place visuellement, dérive le regard de l’important (l’objet) pour le fixer sur ce détail ;
- Les matériaux et les techniques utilisés ne sont pas les bons : pour la réalisation d’une sphère, l’impression 3D exige la décomposition en facettes du volume = assemblage de différents éléments + pliures nombreuses. Or l’objet de référence, PRATONE, tout comme l’aspect médusé que je souhaite obtenir, demandent un objet plus organique, plus léger ;
- dimensions générales de l’objet produit : objet surdimensionné pour l’usage souhaité ;
- Le dessin des pieds en formes de tentacules flexibles est intéressant, il participe du dynamisme recherché. La question se pose de la réalisation.


## Phase 2

### a. Esquisse

![](images/projet%20final/9.jpg)
![](images/projet%20final/13.jpg)

Une forme flottante, dynamique. Le câblage électrique est intégré comme élément du projet. Il est confondu dans les tentacules. L’arrivée du câble et de la douille e27 dans l’objet est travaillée pour faire partie intégrante de l’objet. Jeu avec le câble. D’autres câbles électriques sont utilisés pour faire d’autres tentacules.

### b. 1er test
Un test d’impression de la partie supérieure du corps transparent est réalisé, pour apprécier le rendu de transparence. L’impression est réalisée sur une Ender Reality.
Filament PET-G transparent de la marque UNIC.
Lors de la modélisation sur Fusion 360, j’ai décidé d’avoir une coque de 2mm d’épaisseur. Cette épaisseur me permettra, je pense, un bon compromis entre transparence et solidité.
Paramètres d’impression :
-	3 couches, c’est-à-dire 0,6 mm d’épaisseur de couche de par et d’autre de la coque de l’objet ;
-	Remplissage avec motif giroïdal ;
-	Pas de support.

![](images/projet%20final/c1.1.jpg)

L’impression sans support était risquée, mais j’avais confiance dans la forme du dôme. La surface extérieure est d’ailleurs très nette et propre.
La surface intérieure quant à elle est marquée par le manque de support, des filaments se décrochent.
La transparence nette n’est pas obtenue, l’objet a une translucidité relative.
A noter que l’impression ne s’est pas réalisée de manière égale suivant la hauteur de l’objet, on obtient 3 zones distinctes, avec apparition dans la zone médiane de la texture giroïdale du remplissage.

L’effet obtenu n’est pas celui escompté. Toutefois l’apparition surprenante du motif giroïdal en transparence est intéressante et pourrait participer la poétique de la méduse. La translucidité est assez bonne, sans être excellente lorsque la lumière est éteinte ; lorsqu’elle est allumée ça fonctionne bien. La solidité manifeste de l’objet est convaincante.
L’effet lumineux est intéressant, la succession des trois textures différentes dont une avec motif giroïdal donne un bel effet. 

### c. Divers tests

tests avec une coque de 1mm d’épaisseur : meilleure translucidité, mauvaise solidité, changement de texture (une zone basse translucide, une zone haute blanchâtre)

![](images/projet%20final/11.jpg)

tests avec 2mm d’épaisseur mais 1 seule couche périphérique pour assurer la transparence : le filament ne fixe pas bien par endroit. Impression avortée.

![](images/projet%20final/12.jpg)

tests de tentacules perlés (dessins) : sorte de perles imprimées en 3d + câble en cuivre de grosse section à l’intérieur. Tests mis de côté pour ne pas rajouter un élément de langage supplémentaire qui perturberait l’œil. 

![](images/projet%20final/14.jpg)

test câbles électriques de section 6mm² : excellente rigidité, bonne déformation sous l’effet de la main + maintenu de la forme : peut très clairement supporter le poids de la future lampe sans se déformer. 

![](images/projet%20final/15.jpg)

tests de réceptacle à tentacule + tentacule en câble électrique de grosse section (6mm²) : tests de diamètres internes différents pour voir lequel reçoit le mieux le câble. Alors que le diamètre du câble (gaine plastique incluse) est de 4,1mm, le câble ne rentre qu'à partir de trous de diamètre 4,7mm.

![](images/projet%20final/16.jpg)


# FINAL

Acceptation de la matière, des défauts et des qualités de son traitement comme force du projet (virage dans le projet) : ineffable à la transparence ? la transparence révèle tout, d’autres tests auraient pu créer un objet « parfait », mais l’accident a révélé des qualités.

## Modélisation sur Fusion 360

### a. Corps Translucide

Fichier STL téléchargeable [ici](images/projet%20final/Méduse_corps_transparent.stl)

![](images/projet%20final/3d_1.jpg)

![](images/projet%20final/3d_2.1.jpg)

![](images/projet%20final/3d_3.0.jpg)

![](images/projet%20final/3d_3.1.jpg)

### b. Corps noir

Fichier STL téléchargeable [ici](images/projet%20final/Méduse_partie_ventrale_noire.stl)

![](images/projet%20final/3d_noir_1.jpg)

![](images/projet%20final/3d_noir_2.jpg)

![](images/projet%20final/3d_noir_3.jpg)

![](images/projet%20final/3d_noir_4.jpg)

![](images/projet%20final/3d_noir_5.jpg)

![](images/projet%20final/3d_noir_6.jpg)


## Impression 3d

### a. Corps Translucide

#### Filament PET-G

On a pour habitude d'utilisé sur les imprimantes 3D du filament PLA pour sa facilité d'impression. Ici la translucidité de l'objet demande d'utiliser un filament transparent, inexistant en PLA. Mon choix c'est porté sur le PET-G. Il est connu pour son bon compromis entre robustesse et facilité d'impression. Ce filament ne subit pas de rétractation de matière lors de l'impression, ce qui me permettra d'imprimer la pièce en plusieurs parties et de les coller sans crainte qu'elles ne se touchent parfaitement (ce choix d'imprimer en plusieurs parties est explicité plus bas).

#### Réglages Imprimante

Pour l'impression des différentes parties du corps translucide les réglages seront les suivants :

![](images/projet%20final/réglages_PET-G.jpg)

Réglages propres à l'utilisation du filament PET-G. Le fournisseur du filament (UNIC) conseille une température de buse entre 195 et 220°C. Toutefois je fais confiance aux réglages de Prusa. En effet, après plusieurs essais la température de 220°C ne semble pas suffisante : le filament a du mal à s'accrocher.

![](images/projet%20final/réglages_PET-G_1.jpg)

Réglages généraux d'impression.

#### 1ère solution : impression de l'objet entier

![](images/projet%20final/gif.gif)

Cette solution, à première vue plus simple, pose en fait nombre de problème :
- nécessité de support et donc temps et matière nécessaires considérablement augmentés.
- bugs réguliers lors de l'impression de support, faisant parfois échouer toute l'impression.
- si un bug apparaît à un moment ou un autre de l'impression, on doit tout recommencer, sans garantie de réussite par ailleurs : et donc augmentation considérable de temps d'impression et de matière. 

#### 2ème solution : impression en plusieurs parties

Cette solution permet de palier à tous les problèmes exposés précédemment. De plus, le PET-G est connu pour ne subir aucune rétractation de matière lors de l'impression se qui permet de produire sans crainte plusieurs pièces à assembler. Cette solution sera donc privilégiée pour mes impressions.

**La forme est découpe en trois parties, et ce de manière à supprimer tout besoin de support.**

![](images/projet%20final/c1.jpg)

![](images/projet%20final/c2.jpg)

![](images/projet%20final/c3.jpg)

#### Résultat

![](images/projet%20final/c.jpg)

### b. Corps Noir

#### Réglages Imprimante

Mis à part le changement de filament pour un retour au PLA noir, les réglages restent les mêmes que pour le corps translucide.

Réglages PLA :

![](images/projet%20final/réglages_noir.jpg)

#### Impression

![](images/projet%20final/corps_noir.jpg)

![](images/projet%20final/noirrr.jpg)

## Assemblage

![](images/projet%20final/asse1.jpg)

![](images/projet%20final/asse_2.jpg)

![](images/projet%20final/asse_3.jpg)

![](images/projet%20final/asse4.jpg)

