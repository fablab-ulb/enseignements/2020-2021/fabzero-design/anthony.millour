# Module 2 : Conception assistée par ordinateur

Lors de ce module nous avons dû nous familiariser avec **Fusion 360**, un logiciel de modélisation 3d de la firme Autodesk. L’objectif est de modéliser l’objet que nous avons choisi dans la collection du ADAM, afin de l’imprimer plus tard (lors du module 3) à l’aide d’une imprimante 3d.

## Installation de Fusion 360

Pour une utilisation personnelle (non commerciale), il est possible de télécharger le logiciel sous le lien suivant : [Fusion 360 for personal use](https://www.autodesk.com/products/fusion-360/personal)

## Modélisation de l’objet PRATONE

### Données du Constructeur

Afin de préparer la modélisation, je suis allé voir sur le site de [GUFRAM](https://www.gufram.it/en/prodotto-14-pratone), le fournisseur, si des données techniques étaient disponibles, comme c’est souvent le cas avec les objets de design possédant une certaine notoriété. Quelque fois même des modélisation 3d sont disponibles, ainsi que des fichiers autocad.
J’ai pu trouver un élévation, ainsi qu’une vue de dessus, toutes deux côtés.

![plans orginaux](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/PRATONE-Gufram-128812-dim72f28aef.jpg)

J’ai importé l’image sur Autocad, l’ai mis à l’échelle, et est tiré une multitude de cotations pour plus de précision.

![plans retouchés](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/plans.jpg)

### Schématisation

Pour modéliser efficacement un objet complexe en 3d, il convient premièrement de le décomposer en volumes primitifs, puis d’imaginer les opérations qu’il sera nécessaire d’effectuer pour passer des volumes primitifs à des volumes complexes se rapprochant le plus possible de la réalité :

* un socle :
  1. un parallélépipède rectangle
  2. une soustraction d’un autre volume arrondi, répétée trois fois sur l’une des faces du parallélépipède
  3. une soustraction du nouveau volume obtenu sur la face opposée du même volume pour créer un effet puzzle (emboîtement parfait)
  4. tronquer les arrêtes  
  


* des brins d’herbe :
  1. un parallélépipède rectangle
  2. resserrement de deux arrêtes parallèles au sommet
  3. création et déplacement de certains points pour arrondir/sculpter la forme
  4. pour finir, multiplication et rotation du volume sur le « socle »

### Modélisation 3d

#### Le socle

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/ezgif.com-gif-maker.mp4)

#### Les brins d’herbe

Vu la géométrie de la forme que je dois réalisé je dois m'initier au **SCULPT**. Cette méthode permet de réaliser des formes plus complexe sur Fusion 360, en sculptant l'objet. 

##### Tuto SCULPT Youtube

[https://www.youtube.com/watch?v=OpY6OQQE2XM](https://www.youtube.com/watch?v=OpY6OQQE2XM)

##### A mon tour !

Pour me simplifier la tache je choisi de travailler dans une nouvelle fenêtre ("Nouvelle Conception"). Je rassemblerai plus tard le brin d'herbe nouvellement créé avec le socle.

A savoir ! Dans la dernière version de Fusion 360 il suffit de cliquer sur une des primitives disponibles dans ma barre "CREER" pour ouvrir le mode SCLUPT.

Une vidéo vaut bien toutes les explications du monde :
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/brin_d_herbe.mp4)

Dans la vidéo, je fais quelque fois des aller-retour entre deux modes de visualisation disponible dans le mode SCULPT de Fusion 360 : le mode "sans lissage" plus pratique pour travailler (ALT+1), et le mode "lissé" indispensable pour visualiser les effets des modifications sur la forme (ALT+3). Un troisième mode de vue intermédiaire est disponible (ALT+2).

![lissage](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/avec_ou_sans_lissage.jpg)

L'outil "PLIER" disponible dans la Barre "MODIFIER" me permet de garder des arrêtes vives malgré le lissage :

![plier](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/plier.jpg)

##### PRATONE

Je retourne sur la fenêtre de modélisation du socle, et j'ouvre la bibliothèque pour pouvoir importer mon brin d'herbe (icône tout en haut à gauche, entourée en rouge sur le screen ci-dessous).

![bibli](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/biblioth%C3%A8que.png)

Pour importer le brin d'herbe, rien de plus simple : il suffit de faire glisser l'objet dans le modèle. Je le place sur mon socle, et le copie-colle le nombre de fois que nécessaire, en effectuant une rotation de 90° à chaque fois. Malheureusement'outil "Réseau" ne m'est ici d'aucune utilité, puisque la rotation doit être effectuée.  
Après quelques manips voici le résultat :

![final](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%202/final.jpg)