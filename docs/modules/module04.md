# MODULE 4 : Découpe assistée par ordinateur

## 1. A SAVOIR SUR LA LASER CUTTER

La Laser Cutter mise à disposition dans le FabLab pour cet exercice est une **Lasersaur**, un modèle open-source de découpeuse laser.
Toutes les infos détaillées sur son utilisation se retrouvent sous [FabZero/Machines/files](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/tree/master/files).

#### a. Formats pris en charge par DriveBoardApp, interface de la Laser Cutter

Uniquement les formats vectoriels.

* **SVG**. Fonctionne très bien, toutefois couleurs en RVG. Ce format d'exportation est disponible sur Illustrator et Inskape (Open Source) ;
* DXF. Moins conseillé, nombreux problèmes de compatibilité. Ce format d'exportation est disponible sur AutoCAD.

#### b. Matériaux

* **Recommandés :** Bois contreplaqué (plywood, multiplex) - Acrylique (PMMA, Plexiglass) - Papier, Carton - Textiles ;
* **Déconseillés :** MDF - ABS, PS - PE, PET, PP - composites à base de fibre - métaux ;
* **Interdits :** PVC - Cuivre - Téflon - Vinyle, simili-cuir - Résine phénolique, époxy.


#### c. Réglages et Caractéristiques machine

* **Surface de découpe :** 122x61 cm
* **Epaisseur max de la planche à découper au laser :** 12 mm

* **Puissance max :** 100 W
* **Vitesse :** Généralement on utilise une vitesse autour des 2000 mm/min, mais peut aller jusqu'à 6000 mm/min.

* **Réglage de la distance Tête du laser/Matériau à découper :**
  * si épaisseur matériau < 4 mm : 15 mm
  * si épaisseur matériau > 4 mm : 15 mm - le demi-épaisseur du matériau. Cela permet d'avoir une découpe égalitaire sur la face supérieure et comme sur la face inférieure.

![15mm](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/distance_laser_planche.jpg)

#### d. Avant de lancer une découpe !

* Savoir où se trouve le **bouton d'arrêt d'urgence** + **l'extincteur de CO2** ;
* Allumer le **Refroidisseur** ;
* Ouvrir l'**Arrivée d'air comprimé**. Sert à souffler les poussières loin de la zone de découpe ;
* Allumer les deux **extrateurs d'air**. Ils permettent d'évaquer les gazs toxiques produits par la découpe ;
* Vérifier via l'interface DriveBoard que la totalité de la découpe s'effectue bien sur notre planche ;
* Fermer le capot de la machine. Ses vitres teintées nous permettrons d'observer directement le laser effectuer les découpes, sans avoir à porter des lunettes de **protection des UV**. (Attention ! ce n'est pas le cas de toutes les machines) ;
* C'est parti !

![sécu](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/s%C3%A9cu.jpg)

## 2. UNE FEUILLE DE POLYPROPYLENE + UNE LASER CUTTER = UNE LAMPE !

L'objectif du module 4 est d'explorer les fonctions de la Lasersaur, via un exercice simple : réaliser un abat-jour pour une source de lumière, en s'inspirant d'une des caractéristiques de l'objet d'étude (pour moi PRATONE). Une feuille de polypropylène de 70x51 cm et de 1 mm d'épaisseur nous est fournie.

Caractéristiques du matériau :
* le polypropylène (PP) est un thermoplastique, dont l'utilisation est déconseillée avec un Laser Cutter (émanation de gazs toxiques)
* relativement souple
* translucide
* un côté lisse / un côté texturé (légérement granuleux)

### Inspiration de PRATONE

![PRATONE](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/Gufram-Pratone.jpg)

Après étude de l'objet j'ai décidé de me porter sur la notion de **Détournement**. D'autres sous-notions viendront alimenter le projet telles que **Action/Ludique** et **Unitaire**.

En effet ce qui m'a le plus frappé sur le fauteuil PRATONE c'est bien cette notion de détournement qui le rend si imprévisible, qui fait que l'on ne comprend pas bien sa fonction, son but, avant de l'avoir expérimenté.
Conservoir une assise comme un carré de pelouse surdimensionné, c'est aussi mettre en difficulté nos habitudes, et offrir de nouvelles manières de s'asseoir.

La mousse, tentée de vert, unique matériau de PRATONE se prête bien à la réalisation d'un pareil fauteuil. Sa souplesse et son élasticité répondent aussi bien la fonction qu'à l'image que l'on a d'une végétation confortable dans laquelle on se love en écartant les feuillages. **Il me fallait trouver un objet du quotidien à détourner, qui corresponde aux caractéristiques physiques de mon matériau, le polypropylène** : Translucidité, Plasticité, Souplesse, Légéreté. Très vite c'est vers le **sac plastique** que je me suis focalisé.

![croquis](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/croquis_d_intention.jpg)

Il s'agit d'un objet du quotidien, produit un masse, tellement multiplié, tellement accessible économiquement, qu'on le consomme sans même y prêter attention. Chaque appartement possède un placard discret où ils s'entassent, toujours plus nombreux, **dans l'obscurité**. Lorsqu'on en a trop, on en jette quelques-uns, non sans une pointe de remord qu'on étouffe si vite.  
C'est un objet de second rôle, délaissé. Il n'est pas fait pour briller, il n'est pas fait pour durer. Et pourtant quelqu'un l'a dessiner. Et si aucun être humain ne l'a fabriqué de ses mains, probablement, des ouvriers ont par le passé montés la machine qui l'a fabriqué. Aujourd'hui d'autres ouvriers de 8h30 à 17h font tourner la machine. Du pétrole est extrait, du plastique est produit.
Tout cela pour un destin funeste.

Un sac plastique pour durer, un sac plastique pour briller. Il aura fallu le déposséder de sa fonction première pour le voir briller au grand soir. Si ce luminaire est libéré de la fonction de son cousin le Sac Plastique de porter les courses du magasin à la maison, il sous-entend un dessein proche : il contient la lumière, et vous suggère de la déplacer, quitte à la prendre partout avec vous. C'est la lumière qui vous suit. Changement de paradigme. La lumière se porte et se transporte.


## 3. PROCESSUS DE FABRICATION

### a. Dessin du patron

Un patron est conçu sur AutoCAD. Je me suis fixé une règle simple, le luminaire doit être réalisé en une pièce unique, aucun assemblage avec d'autre élément - pour plus d'**unité**, tout comme PRATONE a cette force d'émerger en un seul morceau.

Le dessin a évolué pour obtenir un objet final mieux "fini", plus lisible. Les zones d'encoches ont été déplacées pour rendre invisibles les jonctions.

![autoCAD](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/patron_AutoCAD.jpg)

Différents calques ont été formés. Un calque pour simplement tracer, et d'autres calques pour découper avec différents réglages de puissance et de vitesse du laser. Et pour cause : lors de la découpe, le laser met un certain temps à atteindre la vitesse qu'on lui assigne. Pour mieux comprendre, prenons le cas d'une voiture. Lorsque l'on démarre et appuit sur l'accélérateur il faut un certain temps pour atteindre la vitesse souhaitée en ligne droite. Dans les virages, le véhicule ralentit brutalement, avant de pouvoir réaccéléré dans la prochaine ligne droite.
Ainsi pour les petits tracés, le laser n'atteindra peut-être jamais la vitesse assignée, il faut donc augmenter la puissance pour compenser.  

Le choix des réglages de vitesse et de puissance peut s'avérer alors quelque peu hasardeux. J'ai donc préférer sortir un dessin test, plus rapide à éxécuter et moins consommateur de matière. Cela me permettra de bidouiller à l'infini les différents réglages jusqu'à atteindre le résultat souhaité pour chaque calque.

Le fichier est enregistré au format DXF, compatible avec Illustrator. Le format DXF est disponible sous "Enregistrer sous"/"Autres formats", et non sous "Exporter". Pour garantir une meilleure compatibilité on séléctionne le format DXF le plus ancien, pour moi DXF R12/LT2.


### b. Exportation en SVG

Comme dit précédemment Inskape permet de convertir le fichier au format SVG, plus compatible avec DriveBoardApp, l'interface de la Lasersaur. Comparé à Illustrator, il a l'énorme avantage de permettre de repositionner facilement le dessin par rapport à l'origine du laser (coin supérieur gauche).

En réalité, pour je ne sais quelle raison, il a été impossible pour moi d'ouvrir un fichier DXF avec Inskape.

Tant pis, j'ai donc testé de le faire sur Illustrator. Et là ça fonctionne !


En fait rien ne fonctionnera à l'étape suivante (passage sur DriveBoardApp), et s'en suit une longue traversée du désert au cours de laquelle j'ai bien failli devenir fou. Après enregistrement sous le format SVG, et ouverture du fichier sur DriveBoardApp, je me rend compte que l'échelle du dessin AutoCAD n'a pas été conservée. Avec l'une des assistantes nous avons tout essayé : 
* ouvrir le fichier SVG sur Inskape pour le ré-enregistrer en SVG : les couleurs des calques n'étaient pas conservées. Toutefois l'échelle était bonne ;
* repartir du fichier AutoCAD et le ré-exporter sous les différents formats DXF (DXF R12, DXF 2000, etc.). Toujours les mêmes problèmes ;
* changer les couleurs des calques ;
* sur AutoCAD, grouper les tracés d'un même calque ;
* utiliser l'ordinateur de quelqu'un d'autre, pour qui tout fonctionne ;
* etc. La liste est longue et j'ai perdu le fil.

Beaucoup de temps à juste tester des choses sans réellement savoir ce qui se passait. Si d'aussi grosses erreurs arrivent plus rarement, de petits problèmes de compatibilité sont fréquents. Les logiciels utilisés (DriveBoardApp et Inskape) sont des logiciels Open-Source, c'est-à-dire qu'ils sont libres de droit et sont généralement conçus par des amateurs très pointus, ou même des professionnels mais sans logique économique derrière. Le suivi des logiciels et les mises à jour sont donc largement moins fréquents et il est normal que des buggs persistent.

Toutefois au bout d'un moment (1h30 quand même), dans toute cette confusion d'aller-retour entre logiciels et formats, un essai fonctionne finalement. **Il semblerait que ce qui différencie cet essai de tous les autres soit d'avoir "Exporter" le fichier en SVG d'Illustrator, et non pas de l'avoir "Enregistrer sous" en SVG**.

### c. DECOUPE LASER

Le fichier SVG est transféré sur une clé USB que l'on branche sur le boîtier près de l'écran d'ordinateur. Rien. Encore une galère.. Je finis par débrancher le boîtier et mets ma clé directement sur le circuit imprimé derrière. Cette fois-ci c'est bon ! Je lance DriveBoardApp dans l'Explorer et ouvre mon fichier. DriveBoardApp est un logiciel qui s'ouvre dans l'Explorer, et nécessite donc une connexion WiFi pour fonctionner en lien avec la Lasersaur.

![drive](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/DriveboardApp1.jpg)

#### TEST
Je commence par mon fichier test. A chaque couleur de mon fichier je fais correspondre un passage "**PASS**" et donc des réglages différents de vitesse et puissance du laser. En discutant avec d'autres élèves et l'assistant, j'ai pu me faire une idée assez précise des réglages dont j'aurais besoin pour mes différents calques :
* **PASS 1** (tracé) : F=1500 et %=22
* **PASS 2** (petits cercles découpés) : F=1500 et %=50
* **PASS 3** (petites découpes) : F=750 et %=23
* **PASS 4** (longues découpes) : F=1500 et %=40

NB : on place toujours le calque de Tracé, avant les calques de Découpe. En effet lors des découpes il peut arriver que le matériau bouge un peu. Il est donc préférable de s'occuper des tracés en premier.

Maintenant que mon fichier est paramétré, je vérifie que [tout est en place pour lancer la découpe](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/s%C3%A9cu.jpg) (bouton d'arrêt d'urgenge/Refroidisseur/Air comprimé/Extracteurs). Je cale la feuille de polypropylène dans la Lasersaur. Je vérifie que la futur découpe tient bien sur ma planche, en appuyant sur **les flêches** à côté de "**Run**". Très pratique, je peux également envoyer le laser à un point précis de mon dessin grâce au bouton "**Move**".  
Je remets le laser à son origine en appuyant sur la "**Maison**". Je ferme le capot, le bouton "**Status**" de DriveBoardApp devient alors **vert**. Je peux lancer la découpe en cliquant sur "**Run**".

Une fois la découpe finie, j'attends quelques instants que la fumée dégagée par l'opération ne soit aspirée avant d'ouvrir le capot.
A ma grande surprise tout est bon du premier coup, les découpes sont propres, quelques traces de cramé, mais dans l'ensemble ça va. Le laser était peut-être un poil trop puissant pour le Tracé.

![test](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/test.jpg)

#### DECOUPE DU PATRON & ASSEMBLAGE 

Je charge donc mon fichier sur DriveBoardApp, introduit les réglages pour chaques calques en prenant en compte les résultats précédents : je baisse un peu l'intensité du laser pour le calque Tracé, et l'augmente un peu pour le tracé des longues découpes pour être sûr de ne pas avoir besoin d'un cutter pour finioler après coup.

![dd](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/drive_lampe.jpg)

La découpe est un succès. A noter malgré tout, toujours la présence de ces quelques traces de cramé, mais rien de trop visible.

![final](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%204/final.jpg)


