# Module 3 : Impression 3D

Maintenant que la modélisation de l'objet est faite sur Fusion 360, nous allons voir comment l'imprimer. Nous travaillerons avec les outils de la firme [**PRUSA**](https://www.prusa3d.fr/), fondée par Josef Prusa. Prusa a développé un concept simple : ses imprimantes impriment d'autres imprimantes, qui à leur tour impriment d'autres imprimantes.
Le FabLab possède une série de ces imprimantes, le modèle **Original Prusa i3 MK3S** pour être précis.

![impri](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/imprimante_3d.png)

Un logiciel, **PrusaSlicer**, permet de préparer le modèle 3d à l'impression. Il s'agira de rentrer les caractéristiques d'impression : épaisseur de la couche extérieure, motif du remplissage, renforts éventuels, etc.

## PRUSASLICER ![logo prusaslicer](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/PrusaSlicer_logo.png)

Le logiciel est disponible [ici](https://www.prusa3d.fr/prusaslicer/). Lors de l'installation on devra spécifier le modèle d'imprimante 3d que l'on utilisera (ici la **Original Prusa i3 MK3S**), les réglages de PrusaSlicer différant d'un modèle à l'autre.

Avant tout, il faut exporter le fichier 3d en **STL** sous Fusion 360. Il nous suffira ensuite d'importer le modèle sous PrusaSlicer. 

### Plateau

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/2020-11-16.jpg)

Le logiciel se lance immédiatement sur l'onglet "**Plateau**". D'autres onglets sont disponibles nous y reviendront plus tard. On retrouve le plateau de l'imprimante 3d **Original Prusa i3 MK3S**.
L'objet ayant été modélisé sur Fusion 360 à l'échelle 1:1 (soit 140 * 140 * 95 cm), j'en réduis l'échelle pour qu'il tienne sur le plateau de l'imprimante. Je régle la largeur sur 10 cm, le reste se met proportionnellement à l'échelle.

On veillera à sélectionner :
* l'impimante **Original Prusa i3 MK3S** ;
* le filament utilisé au FabLab, le **Prusament PLA**.

Puis, si besoin on pourra positionner le modèle sur le plateau de manière à simplifier l'impression : certains modèles nécessiteront d'être retourné dans un sens précis afin de limiter les portes-à-faux, que l'imprimante ne sait gérer que par la création de "**Supports**" (sorte de maillage plus ou moins facile à retirer à postériori permettant de soutenir un porte-à-faux lors de l'impression).
Pour ma part, l'objet modélisé ne nécessite pas cette étape.

### Réglages d'impression

Le but ici est de régler l'impression de manière à ce que l'objet soit suffisemment solide, tout en évitant d'avoir une durée d'impression complétement incohérente.  

#### Couches et Périmètres
Par défaut, on réglera la hauteur de couche sur 0,2 mm. On réglera le **Périmètre** des **Parois verticales** sur 3. 2 serait tout à fait suffisant en terme de résistance, mais les parois par leur finesse laisseraient alors passer la lumière. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/r%C3%A9glage_d_impression_1.jpg)

#### Remplissage
Dans la plupart des cas on choisira un remplissage entre 10 et 15%. Toutefois dans certains cas peuvent nécessiter qu'on monte à 35%.
Dans mon cas je me contente de 10%. Les brins d'herbes étant peu creux (petits objets aux parois très rapprochées) et relativement triangulés, je ne devrais pas avoir de soucis de résistance mécanique. Je choisis le remplissage en **Grille**, dans l'espoir de gagner du temps lors de l'impression.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/r%C3%A9glage_d_impression_2.jpg)

#### Autres réglages
D'autres réglages sont à envisager suivant les cas :
- **Jupes et Bordures** : permet de créer une bordure stabilisant l'objet sur le plateau durant son impression. Impératif si la géométrie de l'objet le rend instable. La bordure est très facile à retirer après coup ;
- **Supports** : les supports permettent de supporter l'impression des porte-à-faux. Ils ne sont pas toujours aisés à retirer après coup suivant la géométrie de l'objet (recoins).

Mon objet ne nécessite pas ces réglages.

### Aperçu & Exportation

Lorsque tous les réglages sont faits, nous pouvons retourner sur l'onglet **Plateau**. On clique sur "**Découper maintenant**" dans la barre de droite, pour lancer la conversion du modèle 3d en modèle imprimable. A l'aide de la barre de temps sur la droite (en rouge) on peut observer la constitution de l'objet et sa construction phase après phase :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/gif_fabrication.gif)

Le logiciel calcule la durée d'impression : 20h59min. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/dur%C3%A9e_d_impression.jpg)

20h59min. Ca fait beaucoup. J'ai vu avec les assistantes pour tenter de réduire ce temps en jouant sur les réglages. Sans effet. Je me résous à jouer sur l'échelle de mon objet pour gagner du temps. Initialement j'avais réglé mon objet sur les dimensions suivantes : 100 * 100 * 67,38 mm ; soit une échelle de 7,14%. Je réduis l'échelle à 5% : le durée d'impression passe à 10h23min. C'est encore beaucoup, mais je ne me vois pas réduire encore l'échelle du fauteuil. Après tout 10h c'est une nuit, si je lance l'impression en fin de journée, je pourrais le récupèrer demain matin.

J'exporte le **G-code** (fichier nécessaire à l'impression).

## L'IMPRESSION 3D

L'imprimante **Original Prusa i3 MK3S** possède un port SD. On transfère donc le fichier G-code sur une carte SD, avant de l'introduire dans l'imprimante.

Lors que l'on branche la carte SD, un menu apparaît permettant de sélectionner notre fichier. On navigue dans le menu à l'aide des boutons à droite de l'écran :
* rotation de la molette : descendre/monter dans le menu ;
* appuyer sur la molette : sélectionner ;
* appuyer sur le bouton "croix" : annuler, retour arrière.

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/%C3%A9cran_imprimante.jpg)

Lorsqu'on lance l'impression, la machine fait **monter la température de la buse** à 215°C, et **celle du plateau** à 60°C. Lorsque ces températures sont atteintes l'**étalonnage** de l'imprimante commence : la buse se déplace sur certains points du plateau et imprime une ligne (! Attention cette ligne est à retirer du plateau avant chaque nouvelle impression !). Puis l'impression commence !

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/impression_en_cours.jpg)

Et le résultat ! L'impression s'est parfaitement déroulée. Même les plus petits détails (tel que la pliure centrale des brins d'herbe) sont correctemment représenté :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/impression_1.jpg)

### Changer de filament/de couleur

Le fauteuil PRATONE est fait pour être additionner. Deux de ses côtés miment la forme d'un puzzle. Dans le but de percevoir cela et de vérifier que mes raccords étaient correctes j'ai procédé à l'impression de deux autres modules, avec le désire de faire varier les couleurs d'impressions chaque fois.

Je détaillerai donc ici la procédure pour changer le filament d'une imprimante 3d, d'après les conseils d'un désormais pilier de l'impression 3d, Dimitri ![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/Sans_titre-1.jpg).

Pour mieux comprendre repartons de cette photo de l'écran de l'imprimante 3d :
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/%C3%A9cran_imprimante.jpg)

Retirer un filament :
1. appuyer sur la molette : on accède ainsi au menu principal
2. sélectionner **Unload filament**, puis **PLA** (pour rappel le PLA est le matériau composant les filaments disponibles au FabLab). S'affiche alors "**Preheating to unload**", la buse chauffe le filament pour que l'on puisse le retirer aisément. Lorsque la température de 215°C est atteinte, le message suivant s'affiche "**press the knob to unload the filament**", on peut alors retirer le filament.
3. il faut couper le bout du filament désormais sorti, et le ranger dans l'un des trous de la bobine, pour ne pas qu'il s'enmêle.

Mettre un nouveau filament :
1. toujours dans le menu principal, sélectionner "**Load filament**, puis **PLA**. S'affiche alors "**Preheating to load**", la buse chauffe pour que l'on puisse introduire le nouveau filament. Lorsque la température de 215°C est atteinte, le message suivant s'affiche "**press the knob to load the filament**", on peut alors insérer le filament. On doit sentir une résistance avant d'arrêter d'enfoncer celui-ci.
2. S'affiche le message "**Filament extruding & with correct color ? Yes/No**", pendant ce temps la buse fait couler se qui lui reste de l'ancienne couleur, puis doit apparaître la nouvelle couleur. Si c'est le cas choisissez "**Yes**", l'on peut alors commencer à imprimer notre fichier. Sinon sélectionner "**No**", et la buse continue jusqu'à faire sortir la bonne couleur.

Voici les quelques modules imprimés et assemblés :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/3_PRATONES.jpg)

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/d%C3%A9tail_de_jonction_entre_deux_modules.jpg)

### Erreurs d'impression

Pour la deuxième impression, j'ai changé le filament noir d'une imprimante pour un filament vert. Plusieurs imperfections sont apparues lors des différentes phases de l'impression : malgré avec respecté la procédure de changement de filament, de nombreuses tâches noires sont présentes à différents niveaux de l'objet. De plus l'impression a produit des nombreuses irrégularités (trous, fins filaments, dépots grossiers) :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%203/impression_rat%C3%A9e_1.jpg)

Il est possible que la buse est conservée des "poches" de l'anciens filaments, qu'elle est relâchées au fur et à mesure de l'impression. Peut être également ai-je oublier de vérifier que la bonne température de buse ait été encodée (215°C).

J'ai donc relancé un impression, en jaune cette fois-ci, en veillant à bien faire couler le filament pendant longtemps cette fois, en en vérifiant la température. Cette fois rien à signaler, à part une légère trace verte sur mon module, mais à peine visible.
