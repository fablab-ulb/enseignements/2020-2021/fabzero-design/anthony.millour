# Module 1 : Gitlab


Dans le présent module, nous verrons comment, pas à pas, je me suis familiarisé avec Git. Le but est de créer une série de pages web servant à documenter mes avancées au sein de l'option **Architecture et Design** sur le modèle de la FabAcademy. Cet apprentissage se décomposera en différents modules. Celui-ci est le premier.  

Les pages web seront hébergées sur Gitlab. Ce site offre deux options :  
* version simplifiée : coder directement sur le site de Gitlab via l'éditeur ;
* version complète : faire un clone dans un dossier sur mon ordinateur des pages hébergées sur le site à l'aide de **GitBash**, un logiciel. Je peux alors les modifier à l'aide du logiciel de traitement de texte **Atom**. GitBash me permettra de faire les liens entre ce dossier-clone et le dossier-origine. Toutes les modifications sont conservées et datées.

**Cette deuxième option offre l'énorme avantage de pouvoir revenir à posteriori sur les différentes modifications apportées aux documents**, ce qui peut être particulièrement pratique lorsque l'on travaille à plusieurs sur un projet, et que chacun apporte ses modifications au fur et à mesure. De plus Atom permet de visualiser en temps réel la page web codée en Mardown.

## **1. cloner le Repository**

Un dossier personnel, appellé **Dépôt** ou **Repository**, a au préalable été créé sur Gitlab par les enseignants de l'option à l'adresse suivante : https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/docs. Comme dit précédemment on peut choisir de travailler directement sur ce dossier et tous les fichiers qu'il contient, ou le cloner sur notre ordinateur pour plus de facilité. Nous allons voir ici comment le cloner.

### Installer GitBash

Pour cloner le Repository il faut Git et ShellBash.

Git c'est :
>"Un **logiciel de gestion de versions** décentralisé, c'est-à-dire un logiciel qui permet de stocker un ensemble de fichiers en conservant la chronologie de toutes les modifications qui ont été effectuées dessus."
 
Ce logiciel est développé sur Linux. **Il faut un invite de commande pour le faire tourner sur Windows ou Mac**. **ShellBash** est un invite de commande qui fonctionne sous Windows. Dans son cours Denis Terwagne, nous propose de d'installer les deux séparemment, et de réaliser toute une série de manip pour se faire. En réalité, cela a été beaucoup plus simple pour moi : une combinaison des deux logiciels a été créé qui se nomme **GitBash**. Il suffit de le télécharger [ici](https://gitforwindows.org/), de l'installer et c'est tout !

### Identification sur GitBash

#### Identifiants

Pour créer un lien entre GitBash et le site web Gitlab.com il faut s'identifier sur GitBash, en utilisant le **username** et l'**email** utilisé sur Gitlab. On lance donc le logiciel. Un invite de commande s'ouvre.

On commence par vérifier que tout fonctionne en tapant la ligne de commande suivante :  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/gitbash_1.jpg)


Puis on passe à la configuration. Ligne de commande :  
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/gitbash_config_nom_et_email.jpg)

La dernière ligne de commande, **git config --global --list** me permet de vérifier que les infos ont bien été rentrées.

#### Clé SSH

Créer une clé SSH permet de connecter son ordinateur local au serveur distant d'une manière sure. On choisira la clé ED25519, une clé SSH plus sure encore.

##### Création de la Clé ED25519

La création de la clé ED25519 se fait sur GitBash, à l'aide de la ligne de commande _**ssh-keygen -t ed25519 -C "clé"**_

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/cr%C3%A9ation_SSH.png)

Une clé publique et une clé privée sont alors créées dans le dossier mentionné dans l'invite de commande : _/c/Users/antho/.ssh_. **C'est la clé publique qui nous intéresse**, nommée **id_ed25519.pub**. On l'ouvre avec le logiciel de traitement de texte Atom. On copie le texte qui apparaît sur Atom.

##### Introduction de la clé ED25519

Maintenant il s'agit d'aller sur le site web de Gitlab, sur notre espace personnel, dans "Paramètres", "Clé SSH". Ici on colle la clé.

On vérifie ensuite que la procédure ait bien marchée avec la ligne de commande **_ssh -T git@gitlab.com_**
![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/authentification_SSH.png)

Gitlab me souhaite la bienvenue, c'est réussit !

### Clonage du Repository

Pour cloner le Repository il suffit de se rendre dessus. Pour moi sous l'adresse https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour. 

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/cloner_le_repository_sur_gitlab.jpg)

On copie le texte sous "Clone with SSH". Il s'agit ni plus ni moins de l'adresse web du Repository. On créer un dossier quelque part sur notre ordinateur, ce dossier contiendra sous peu notre Repository cloné. On ouvre GitBash dans ce dossier. Pour ouvrir GitBash dans un dossier il suffit d'aller dans le dossier, de faire clic-droit et de sélectionner **_GitBash Here_**.
On introduit la ligne de commande suivante : **_git clone_** puis on colle le texte :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/cloner_le_repository.png)

Voilà l'intrégralité du Repository est cloné dans notre dossier sur l'ordinateur.

## **2. coder les pages**

apprendre le langage Markdown
installer Atom
ouvrir le dossier-clone (Repository) sur Atom
apporter des modifications + les enregistrer localement

### Apprendre à coder en Markdown

La rédaction des pages web sur Gitlab se fait en Markdown. Il s'agit d'un langage simplifié permettant de mettre en page texte et images de manière très basique (comme c'est le cas sur Wikipédia par exemple).

Pour apprendre ce langage, je me suis rendu sur le tutoriel intéractif suivant : [https://www.markdowntutorial.com/](https://www.markdowntutorial.com/)
En voici les grandes lignes :

![](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero-design/anthony.millour/-/raw/master/docs/images/MODULE%201/Markdown.jpg)

## 3. exporter les modifs sur Gitlab
utiliser GitBash pour les enregistrer sur le dossier-origine, à l'aide des lignes de commande suivante :
* voir la liste des fichiers à mettre à jour vers le serveur distant (commit) et les fichiers non-traqués (untracked) sur notre Repository cloné : **_git status_**. Les fichiers non-traqués ne seront pas mis à jour sur le Repository distant.
* traquer les fichiers modifiés : **_git add -A_**. Le **_-A_** permet de traquer tous les fichiers non-traqués d'un coup.
* mettre à jour les modifs : **_git commit -m "modif 1"_**. Le **_-m ""_** permet de fixer un commentaire à la mise à jour (pratique pour s'y retrouver lorsqu'on a 15 mises à jour)

tutoriel détaillé https://www.youtube.com/watch?v=TlQbEWHt3pY


